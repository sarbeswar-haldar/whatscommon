<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Open+Sans&display=swap" rel="stylesheet"> 
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/jquery.bxslider.css" rel="stylesheet">

  <!-- ========================================================
  * Template Name: Virtual Event
  * Author: Biswanath hazra
  ========================================================= -->

<body>
  <div class="parallax-window" data-parallax="scroll" data-image-src="img/websiteBg.jpg">
    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex">
      <div class="container-fluid">
        <div class="header-container d-flex align-items-center">
          <div class="logo mr-auto">
            <a href="index.html">
              <img src="img/logo2.png" alt="" class="img-fluid logo1">
              <img src="img/logo.png" alt="" class="img-fluid logo2">
            </a>
          </div>

          <div class="headerRight">
            <a href="#">
              <img src="img/headerLogin.png" class="img-fluid" alt="">
            </a>

            <a href="#">
              <img src="img/headerBar.png" class="img-fluid" alt="">
            </a>
          </div>
        </div><!-- End Header Container -->
      </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center pb-0">
      <div class="container position-relative" data-aos="fade-in" data-aos-delay="200">
        <div class="row platformRow">
          <div class="col-md-7 heroCol">
            <h1>RECONNECTING <br><span class="bannerHeading2" style="color: #93cc66;">FAMILY</span> <span class="bannerHeading2">and</span> <span class="bannerHeading2" style="color: #93cc66;">FRIENDS</span> <br><span class="bannerHeading3">IS NOW POSSIBLE</span> <br><span  class="bannerHeading2">BASED ON</span> <br><span class="bannerHeading2" style="color: #93cc66;">“What’scommon”</span></h1>
            <div class="bannerText2">Because “everyone is looking for someone”, <br>
            what’scommon has made it possible in ways that <br>
            have never done before. Who are you looking for...</div>
              
            <div class="bannerBtn">
              <a href="signup" class="btn-get-started scrollto"><img src="img/bannerBtnLogo1.png" class="img-fluid mr-1" alt=""> First Timer</a>
              <a href="login" class="btn-get-started scrollto"><img src="img/bannerBtnLogo2.png" class="img-fluid mr-1" alt=""> welcome back</a>
            </div>
          </div>
          <div class="col-md-5">
            <div class="bannerImage">
              <img src="img/bannerimg.png" alt="" class="img-fluid">
            </div>
          </div>
        </div>


          
        <div class="platform homeAbout">
          <div class="row content platformRow">
            <div class="col-lg-4" data-aos="fade-right" data-aos-delay="100">
              <h2><span class="platformSmallFont">“WELCOME TO </span> <br><span style="color: #3b71b9;">&nbsp;&nbsp;&nbsp; WHAT’S</span> <br><span style="color: #92d054;"> &nbsp;COMMON”</span></h2>

              <div class="abtBtn">
              <a href="#" class="btn-get-started scrollto">About Us</a>
            </div>
            </div>
            <div class="col-lg-8 pt-4 pt-lg-0" data-aos="fade-left" data-aos-delay="200">
              <div class="homeAboutText">
                what’scommon concept was inspired by a television show more than 10 years ago with a big heart of wanting to reconnect biological parent with their children, then evolved more than 5 years later with the want and desire to reconnect with my military battle buddies, friends and neighbors but simply was impossible with limited information useless within the typical search platforms of today, until now! Welcome to “what’scommon”, Who are you looking for? Who might be looking for you? <span style="color: #93cc66;">“everyone is looking for someone”</span>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </section><!-- End Hero -->

    <main id="main">

      <section id="platform" class="platform">
        <div class="platformOnly">
          
          <div class="row content platformRow">
            <div class="col-lg-7" data-aos="fade-right" data-aos-delay="100">
              <img src="img/ev_manage_platform_bg.png" alt="" class="img-fluid platformImg">
            </div>
            <div class="col-lg-5 pt-4 pt-lg-0" data-aos="fade-left" data-aos-delay="200">
              <h2><span style="color: #000;">some</span> <br>amazing <br><span class="platformFont2">Features</span> <br>THAT’LL <br>change your <br><span class="platformFont2">LIFE!</span></h2>
              
            </div>
          </div>
        </div>

        <div class="downloadBackground">
          <div id="services">
            <div class="container position-relative">
            <div class="sqr">
              <img src="img/sqr.png" class="img-fluid" alt="">
            </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    
                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services1.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Personal</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services2.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Dating</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services3.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Adoption</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services4.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Travel/Vacation</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services5.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Militry</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services6.png" class="img-fluid" alt="">
                          <div class="servicesTabText">School/Education</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services7.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Careers/Job/Work</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services8.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Pets/Animal</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services9.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Reverse Lookup</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services10.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Lost & Found</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services11.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Images Search</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100">
                        <div class="servicesTab">
                          <img src="img/services12.png" class="img-fluid" alt="">
                          <div class="servicesTabText">DNA Match</div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>

            </div>
          </div>

          <div id="downloadSection">
            <div class="container">
              <div class="row content downloadSectionRow">
                <div class="col-lg-6" data-aos="fade-left" data-aos-delay="200">
                  <div class="phBg">
                    <img src="img/downloadPh.png" alt="" class="img-fluid">
                  </div>
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-right" data-aos-delay="100">
                  <h2><span style="color: #000;">get</span> <br><span class="platformFont4">&nbsp;&nbsp; first month</span> <br><span class="platformFont3">free</span></h2>
                
                  <div class="homeAboutText">
                    Enjoy all the powerful benefits of what’scommon for one month FREE on our 100% secure network that uses SSL for end-to-end encryption for your personal information.
                  </div>

                  <ul class="mb-0 list-unstyled appStoreAll">
                    <li class="d-inline-block">
                      <a href="#">
                        <img src="img/appStore.png" class="img-fluid" alt="">
                      </a>
                    </li>
                    <li class="d-inline-block">
                      <a href="#">
                        <img src="img/playStore.png" class="img-fluid" alt="">
                      </a>
                    </li>
                  </ul>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </section><!-- End Services Section -->

      <!-- ======= Testimonials Section ======= -->
      <section id="testimonials" class="testimonials platform">
        <div class="testimonialsBg">
          <div class="container">
            <h2 class="text-center testimonialsHeading">Testimonials</h2>
            <div class="row">
              <div class="col-lg-12" data-aos="fade-up" data-aos-delay="100">

                <ul class="list-unstyled mb-0 testimonialSlider">
                  <li class="d-inline-block testimonialSliderLi">
                    <div class="testimonialQuote">
                      <img src="img/quote_up.png" class="img-fluid" alt="">
                    </div>
                    <div class="testimonialAvtarBody">
                      <div class="testimonialAvtar">
                        <img src="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZmFjZXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80" alt="" class="img-fluid">
                      </div>
                      <div class="homeAboutText">
                        Thank you what’scommon, you made 30 years of looking for my Battle Budy happen with hardley any information! WOW!

                        <div class="testimonialAvtarAddress">Scott A. McCuskey</div>
                        <div class="testimonialAvtarPlace">Texas, USA</div>
                      </div>
                    </div>
                  </li>

                  <li class="d-inline-block testimonialSliderLi">
                    <div class="testimonialQuote">
                      <img src="img/quote_up.png" class="img-fluid" alt="">
                    </div>
                    <div class="testimonialAvtarBody">
                      <div class="testimonialAvtar">
                        <img src="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZmFjZXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80" alt="" class="img-fluid">
                      </div>
                      <div class="homeAboutText">
                        Thank you what’scommon, you made 30 years of looking for my Battle Budy happen with hardley any information! WOW!

                        <div class="testimonialAvtarAddress">Scott A. McCuskey</div>
                        <div class="testimonialAvtarPlace">Texas, USA</div>
                      </div>
                    </div>
                  </li>

                  <li class="d-inline-block testimonialSliderLi">
                    <div class="testimonialQuote">
                      <img src="img/quote_up.png" class="img-fluid" alt="">
                    </div>
                    <div class="testimonialAvtarBody">
                      <div class="testimonialAvtar">
                        <img src="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZmFjZXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80" alt="" class="img-fluid">
                      </div>
                      <div class="homeAboutText">
                        Thank you what’scommon, you made 30 years of looking for my Battle Budy happen with hardley any information! WOW!

                        <div class="testimonialAvtarAddress">Scott A. McCuskey</div>
                        <div class="testimonialAvtarPlace">Texas, USA</div>
                      </div>
                    </div>
                  </li>

                </ul>
              </div>
            </div>

          </div>
        </div>

        <div id="video" class="platform">
          <div class="container">
            <div class="row content downloadSectionRow">
              <div class="col-lg-7" data-aos="fade-left" data-aos-delay="200">
                <div class="vidImg">
                  <img src="img/vid_img.png" alt="" class="img-fluid">
                </div>
              </div>
              <div class="col-lg-5 pt-4 pt-lg-0" data-aos="fade-right" data-aos-delay="100">
                <h2>LET’S TOUR HOW <br>
                <span class="videoTitle2">Whats’</span><span class="videoTitle2" style="color: #92d054;">Common</span> <br>
                WORKS FOR YOU!</h2>
              
                <div class="homeAboutText">
                  You’re just a click away from watching how simple and easy it is to plug in basic details of your life events and combing them the powerful technology of what’scommon to connect to who, what, where, when, why is important to you!
                </div>

                <div class="homeAboutTextVideo2">“everyone is looking for someone”</div>

                <div class="videoPresentationBtn" data-toggle="modal" data-target="#watchvidPop"><img src="img/videoPresentationBtn.png" class="img-fluid" alt=""> &nbsp;Video Presentation</div>
                
              </div>
            </div>
          </div>
        </div>
      </section><!-- End Testimonials Section -->

      <section id="contactUs" class="platform">
        <div class="container">
          
          <div class="row content platformRow">
            <div class="col-lg-4" data-aos="fade-right" data-aos-delay="100">
              <h2><span class="videoTitle2" style="color: #000;"> SHOOT US</span> <br>
              A NOTE</h2>

              <div class="homeAboutText">
                Questions, comment and we certainly love suggestions. We are just a quick note away! No time to be shy, we’d love to hear from you!
              </div>
            </div>
            <div class="col-lg-2 blankCol"></div>
            <div class="col-lg-6 pt-lg-0" data-aos="fade-left" data-aos-delay="200">
              <div class="contactForm">
                <div class="contactFormTitle">Query</div>
                <form action="">
                  <div class="form-group">
                    <input type="text" class="form-control" id="name" placeholder="Name" name="name">
                  </div>
                  <div class="form-group">
                    <input type="number" class="form-control" id="mobileNo" placeholder="Mobile No." name="mobileNo">
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control" id="email" placeholder="Enter Id." name="email">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="subject" placeholder="Subject" name="subject">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="message" placeholder="Message" name="message">
                  </div>
                  
                  <div class="abtBtn">
                    <a href="#" class="btn-get-started scrollto"><img src="img/submitIcon.png" class="img-fluid" alt=""> Shoot It!</a>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="subscribeSection">
            <div class="row content platformRow">
              <div class="col-lg-6" data-aos="fade-right" data-aos-delay="100">
                <div class="subscribeTitle">Subscribe for updates</div>

                <div class="homeAboutText">
                  You’re just a click away for the latest and greatest!
                </div>
              </div>
              <div class="col-lg-6 pt-lg-0" data-aos="fade-left" data-aos-delay="200">
                <div class="subscribeForm">
                  <form action="">
                    <div class="form-group">
                      <!-- <input type="text" class="form-control" id="subscribe" placeholder="Enter email address for updates" name="subscribe"> -->
                      <!-- <img src="img/subscribeIcon.png" class="img-fluid subscribeIcon" alt=""> -->
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter email address for updates">
                        <div class="input-group-append">
                          <button class="btn btn-subscribe" type="submit"><img src="img/subscribeIcon.png" class="img-fluid" alt=""></button>  
                         </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </main><!-- End #main -->

  </div>

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footerTop">
      <div class="row footerRow">
        <div class="col-lg-5 footerCol">
          <div class="googleMap">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3683.1574159553993!2d88.42609151459625!3d22.610596137273934!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a027589b4c398e3%3A0xbf4081ba4c57562f!2s25%2C%208%2C%20VIP%20Rd%2C%20Udayan%20Pally%2C%20Baguihati%2C%20Kolkata%2C%20West%20Bengal%20700159!5e0!3m2!1sen!2sin!4v1613736098009!5m2!1sen!2sin" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
          </div>
        </div>
        <div class="col-lg-7 footerCol">
          <div class="footerDetails">
            <div class="row">
              <div class="col-lg-1 blankCol"></div>
              <div class="col-lg-6 footerDetailCol">
                <div class="footerDetailsHeading">Contact Us</div>
                <div class="footerDetailsAddress">A-25/8, VIP Park, Rabindrapally, <br>Kestopur Kolkata, WB 700101</div>
                <div class="footerDetailsText footerDetailsPh">+91 98365 98366  &nbsp;/&nbsp;  +91 89657 32145</div>
                <div class="footerDetailsText footerDetailsEmail">info@whatscommon.com</div>

                <ul class="socialLink mb-0 list-unstyled">
                  <li class="d-inline-block">
                    <a href="#" target="_blank">
                      <img src="img/facebook.png" class="img-fluid" alt="">
                    </a>
                  </li>

                  <li class="d-inline-block">
                    <a href="#" target="_blank">
                      <img src="img/youtube.png" class="img-fluid" alt="">
                    </a>
                  </li>

                  <li class="d-inline-block">
                    <a href="#" target="_blank">
                      <img src="img/linkdin.png" class="img-fluid" alt="">
                    </a>
                  </li>

                  <li class="d-inline-block">
                    <a href="#" target="_blank">
                      <img src="img/instra.png" class="img-fluid" alt="">
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-3 footerMenuCol">
                <div class="footerDetailsHeading">Quick Links</div>

                <ul class="footerMenu mb-0 list-unstyled">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">About</a></li>
                  <li><a href="#">Features</a></li>
                  <li><a href="#">Testimonials</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    

    <div class="footerBottom">
      <div class="container d-md-flex">

        <div class="copyblink mr-md-auto text-center text-md-left">
          <div class="copyright">
            &copy; whatscommon <span id="footerYear"></span> &nbsp;|&nbsp; All Rights Reserved.
          </div>

          <div class="bottomlink">
            Designed By<a href="https://zabingo.com"> Zabingo.com</a>
          </div>
        </div>
        
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>

  <div class="modal fade" id="watchvidPop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <iframe 
            width="100%" 
            height="315" 
            src="https://www.youtube.com/embed/nqE4TiDcAz4" 
            frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
            allowfullscreen>
          </iframe>
        </div>
      </div>
    </div>
  </div>

  

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

  <!-- parallax -->
  <script src="assets/js/parallax.js"></script>
  <script src="assets/js/jquery.bxslider.js"></script>


  <script>
    $(document).ready(function(){
      $(".testimonialSlider").bxSlider({
        slideWidth: 356,
        minSlides: 1,
        maxSlides: 8,
        slideMargin: 30,
        controls: false,
        pager: true,
        infiniteLoop: true,
        auto: true,
        autoHover: true,
        speed: 1500,
        moveSlides: 1
      });
    });

    var d = new Date();
    var n = d.getFullYear();
    document.getElementById("footerYear").innerHTML = n;
  </script>

</body>

</html>