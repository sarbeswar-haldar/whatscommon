<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  @include('inc.css')

<body>
  <section id="login" class="signUpBg signUpBg2">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm signUp2Form">
          <div class="loginLogo">
            <img src="img/signUpLogo.png" class="img-fluid" alt="">
          </div>

          <form action="register-step-two" class="signUpForm2 xhr_form" id="signUpForm2">
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="signUpForm2Title">What</div>

                <div class="form-group">
                  <label for="email">Email</label>
                  @if(Session::has('userdata'))
                  <input type="email" class="form-control formMail" placeholder="@gmail.com" name="email"
                   value="{{ Session::get('userdata')->email }}" readonly="">
                  @else
                  <input type="email" class="form-control formMail" placeholder="@gmail.com" name="email" value="">
                  @endif
                  <p class="text-danger" id="email_error"></p>
                </div>

                <div class="form-group">
                  <label for="number">Phone</label>
                  @if(Session::has('userdata'))
                  <input type="number" class="form-control formPh" placeholder="000-000-000" name="phone"
                   value="{{ Session::get('userdata')->phone }}" >
                  @else
                  <input type="number" class="form-control formPh" placeholder="000-000-000" name="phone" value="">
                  @endif
                  <p class="text-danger" id="phone_error"></p>
                </div>

                <div class="signUpForm2Title">Where</div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="number">Country</label>
                      <input type="text" class="form-control formCountry" name="country">
                      <p class="text-danger" id="country_error"></p>
                      <!-- <select class="form-control formCountry" id="" name="">
                        <option>Country</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                      </select>
                      <img src="img/selectIcon.png" class="img-fluid selectIcon" alt=""> -->
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="number">State</label>
                      <input type="text" name="state" class="form-control formState">
                      <p class="text-danger" id="state_error"></p>
                      <!-- <select class="form-control formState" id="" name="">
                        <option>State</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                      </select>
                      <img src="img/selectIcon.png" class="img-fluid selectIcon" alt=""> -->
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="number">City</label>
                      <input type="text" name="city" class="form-control formCity">
                      <p class="text-danger" id="city_error"></p>
                      <!-- <select class="form-control formCity" id="" name="">
                        <option>City</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                      </select>
                      <img src="img/selectIcon.png" class="img-fluid selectIcon" alt=""> -->
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="number">Province</label>
                      <input type="text" class="form-control formProvince" name="province">
                      <p class="text-danger" id="province_error"></p>
                      <!-- <select class="form-control formProvince" id="" name="">
                        <option>Prov.</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                      </select>
                      <img src="img/selectIcon.png" class="img-fluid selectIcon" alt=""> -->
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="text">Street Address</label>
                      <input type="text" class="form-control formAddress" placeholder="123 abc Ave." name="street">
                      <p class="text-danger" id="street_error"></p>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="text">Zip</label>
                      <input type="text" class="form-control formZip" placeholder="Zip" name="zip">
                      <p class="text-danger" id="zip_error"></p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="signUpForm2Title">When</div>
                <div class="birthdayDateTitle">Birthday</div>
                <div class="birthdayDate">
                  <input type="hidden" name="dob" id="dob">
                  <div class="form-group birthdayMonth">
                    <label for="number">Month</label>
                    <select class="form-control formMonth dob" id="month" name="">
                      <option value="" hidden="">Month</option>
                      <option value="01">Jan</option>
                      <option value="02">Feb</option>
                      <option value="03">Mar</option>
                      <option value="04">Apr</option>
                      <option value="05">May</option>
                      <option value="06">Jun</option>
                      <option value="07">Jul</option>
                      <option value="08">Aug</option>
                      <option value="09">Sep</option>
                      <option value="10">Oct</option>
                      <option value="11">Nov</option>
                      <option value="12">Dec</option>
                    </select>
                    <img src="img/selectIcon.png" class="img-fluid selectIcon" alt="">
                  </div>
                  <div class="form-group birthdayDay">
                    <label for="number">Day</label>
                    <select class="form-control dob" id="day" name="">
                      <option value="" hidden="">DD</option>
                      <option value="01">1</option>
                      <option value="02">2</option>
                      <option value="03">3</option>
                      <option value="04">4</option>
                      <option value="05">5</option>
                      <option value="06">6</option>
                      <option value="07">7</option>
                      <option value="08">8</option>
                      <option value="09">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                      <option value="24">24</option>
                      <option value="25">25</option>
                      <option value="26">26</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                      <option value="29">29</option>
                      <option value="30">30</option>
                      <option value="31">31</option>
                    </select>
                    <img src="img/selectIcon.png" class="img-fluid selectIcon" alt="">
                  </div>
                  <div class="form-group birthdayYear">
                    <label for="number">Year</label>
                    <select class="form-control dob" id="year" name="">
                      <option value="" hidden="">YYYY</option>
                      @for ($i = 1950; $i <= 2021; $i++)
                      <option value="{{ $i }}">{{$i}}</option>
                      @endfor
                      
                    </select>
                    <img src="img/selectIcon.png" class="img-fluid selectIcon" alt="">
                  </div>
                  
                </div>
                <p class="text-danger" id="dob_error"></p>

                <div class="signUpForm2Title">Who, What, Where, When, Why</div>

                <div class="addFieldItem">Samgsung <span class="removeField"><img src="img/removeField.png" class="img-fluid" alt=""></span></div>
                <div class="addFieldItem">Dog <span class="removeField"><img src="img/removeField.png" class="img-fluid" alt=""></span></div>
                <div class="addFieldItem">Treehouse <span class="removeField"><img src="img/removeField.png" class="img-fluid" alt=""></span></div>

                <div class="input-group lifeEvent">
                  <input type="text" class="form-control" placeholder="Life Event Keyword(s)">
                  <div class="input-group-append">
                    <div class="btn btn-add">Add</div>  
                   </div>
                </div>
                <div class="inputBellowText">Ex: Word1, Word2, Word3</div>

                <div class="clearfix"></div>
              </div>
              <div class="form-group" id="responseDiv" style="display: none;"></div>
            </div>
            <button type="submit" class="btn btn-proceed">Sign Up</button>
          </form>
        </div>
      </div>
    </div>
  </section>

  @include('inc.script')
  <script>
$(document).on('change','.dob',function(){
  var day = $('#day').val();
  var month = $('#month').val();
  var year = $('#year').val();
  var dob = year+'-'+month+'-'+day;
  $('#dob').val(dob);
});
$(document).ready(function() {
  $(".removeField img").click(function() {
    $(this).parent().parent('.addFieldItem').remove();
  });

  $(".btn-add").click(function() {
    
  });

});

  
  </script>

</body>

</html>