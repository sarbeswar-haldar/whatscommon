<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  @include('inc.css')

<body>
  <section id="login" class="loginBg">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm">
          <div class="loginLogo">
            <img src="img/loginLogo.png" class="img-fluid" alt="">
          </div>

          <form method="post" class="loginFormSection" id="loginForm">
            @csrf
            <div class="form-group loginName">
              <input type="text" class="form-control" placeholder="Email" name="email" required="">
            </div>
            <div class="form-group loginPass">
              <input type="password" class="form-control" id="pwd" placeholder="Password" name="password" required="">
              <img src="img/passwordVisiable.png" class="img-fluid passwordVisiable" alt="">
            </div>
            <div class="form-group" id="responseDiv" style="display: none;"></div>
            <button type="submit" class="btn btn-block btn-welcome">Welcome Back!</button>
            <div class="loginForgot"><a href="forgot-password">Forgot password?</a></div>
          </form>
        </div>

        <div class="loginTextBelow">
          <div class="loginText">By Continuing, you agree to What’sCommon <a href="#">Terms of Service</a>.  We will manage your information as written in our <a href="#">Privacy Policy</a> and <a href="#">Cookie Policy</a>.</div>
        </div>

        <div class="loginForm">
          <div class="loginText">Don’t have an account yet?</div>

          <div class="loginText loginWithText">Login with</div>

          <ul class="loginSocial list-unstyled mb-0">
            <li class="d-inline-block">
              <a href="#"><img src="img/twit.png" alt=""></a>
            </li>
            <li class="d-inline-block">
              <a href="#"><img src="img/tiktok.png" alt=""></a>
            </li>
            <li class="d-inline-block">
              <a href="#"><img src="img/ttt.png" alt=""></a>
            </li>
            <li class="d-inline-block">
              <a href="#"><img src="img/facebookLogin.png" alt=""></a>
            </li>
            <li class="d-inline-block">
              <a href="#"><img src="img/gmail.png" alt=""></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="loginClose">
      <a href="/"><img src="img/loginClose.png" class="img-fluid" alt=""></a>
    </div>
  </section>
  @include('inc.script')
<script type="text/javascript">
$(document).on('submit','#loginForm',function(e){
  e.preventDefault();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  var action = site_url('api/login');
  $.ajax({
    type : "POST",
    data : new FormData(this),
    contentType: false,   
    cache: false,             
    processData:false, 
    url  : action,
    success : function(response){
      console.log(response);
      /*var obj = jQuery.parseJSON(response);*/
      if(response.response_code==200){
        $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
        $(location).attr("href", site_url());
      }else{
        $('#responseDiv').html('<p class="text-danger">'+response.response_msg+'</p>');
      }
    },error: function(jqXHR, textStatus, errorThrown){
    /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
    /*console.log(jqXHR);*/
    console.log(jqXHR.responseJSON);
    $('#responseDiv').show();
    $('#responseDiv').html('<p class="alert alert-danger">'+jqXHR.responseJSON.response_msg+'</p>');
  }
  });
});
</script>
</body>

</html>