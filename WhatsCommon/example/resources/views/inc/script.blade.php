  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

  <!-- parallax -->
  <!-- <script src="assets/js/parallax.js"></script> -->
  <!-- <script src="assets/js/jquery.bxslider.js"></script> -->

  <script>
  $(document).ready(function(){
    $('#login').css('minHeight',$(window).height());
  });
  function site_url(page_name=''){
    var site_url = '<?php echo url('/');  ?>';
    site_url = site_url+'/'+page_name;
    console.log(site_url);
    return site_url;
  }
  $(document).on('submit' ,'.xhr_form' , function(e){
    e.preventDefault();
    var token = $('input[name="_token"]').attr('value');
    console.log(token);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    var submit_id = $(this).attr('id');
    var action    = site_url($(this).attr('action'));
    var submit_btn_content = $('#'+submit_id+' button[type="submit"]').html();

    $('#'+submit_id+' button[type="submit"]').html("<i class='fa fa-spinner fa-spin'></i> Loading");
    $('#'+submit_id+' button[type="submit"]').attr('disabled',true);
    $.ajax({
      type : "POST",
      data : new FormData(this),
      contentType: false,   
      cache: false,             
      processData:false, 
      url  : action,
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          $('#responseDiv').show();
          $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
          /*$(location).attr("href", site_url());*/
          
          if (response.redirect && response.time) {
            window.setTimeout(function(){window.location.href = response.redirect;}, response.time);
          }
          else if(response.redirect){
            $(location).attr("href", response.redirect); 
          }
          else if(response.time){
            window.setTimeout(function(){location.reload();}, response.time);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
        /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
        /*console.log(jqXHR);*/
        console.log(jqXHR.responseJSON);
        $('#'+submit_id+' button[type="submit"]').attr('disabled',false);
        $('#'+submit_id+' button[type="submit"]').html(submit_btn_content);
        if(jqXHR.responseJSON.errors){
          var err = jqXHR.responseJSON.errors;
          $.each(err, function(key, value){

            if (err[key]) {
              $("#"+key+"_error").html(err[key]);
            }
            else{
              $("#"+key+"_error").html(''); 
            }
          });
        }
      }
    });
  });
  </script>