<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  @include('inc.css')

<body>
  <section id="login" class="signUpBg">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm">
          <div class="loginLogo">
            <img src="img/signUpLogo.png" class="img-fluid" alt="">
          </div>

          <div class="signUpHeading">Forgot Password</div>
          <div class="loginText" style="color: #000;">Fill Veryfication code email address</div>

          <form action="check-email" class="signUpForm xhr_form" id="resetForm">
            <div class="row">
              @csrf
              <div class="col-md-10 ml-5">
                <div class="form-group">
                  <input type="email" class="inputText form-control" name="email" required/>
                  <span class="floating-label">Email Address</span>
                  <p class="text-danger" id="email_error"></p>
                </div>

                <div class="form-group" id="responseDiv" style="display: none;"></div>
              </div>
              
            </div>
            <button type="submit" class="btn btn-proceed">Proceed</button>
          </form>
        </div>
      </div>
    </div>
  </section>

  @include('inc.script')

</body>

</html>