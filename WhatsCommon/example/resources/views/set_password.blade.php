<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  @include('inc.css')

<body>
  <section id="login" class="signUpBg">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm">
          <div class="loginLogo">
            <img src="img/signUpLogo.png" class="img-fluid" alt="">
          </div>

          <div class="signUpHeading">Change Password</div>
          <div class="loginText" style="color: #000;">Fill in your information</div>

          <form action="set-password" class="signUpForm xhr_form" id="setPassword">
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" name="otp" required/>
                  <span class="floating-label">Verification Code</span>
                  <p class="text-danger" id="otp_error"></p>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="inputText form-control" name="password" required/>
                  <span class="floating-label">Password</span>
                  <img src="img/passwordVisiable2.png" class="img-fluid passwordVisiable" alt="">
                  <p class="text-danger" id="password_error"></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="inputText form-control" name="confirm_password" required/>
                  <span class="floating-label">Re-type Password</span>
                  <img src="img/passwordVisiable2.png" class="img-fluid passwordVisiable" alt="">
                  <p class="text-danger" id="confirm_password_error"></p>
                </div>
              </div>
              
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group" id="responseDiv" style="display: none;"></div>
              </div>
            </div>
            <button type="submit" class="btn btn-proceed">Proceed</button>
          </form>
        </div>
      </div>
    </div>
  </section>

  @include('inc.script')

</body>

</html>