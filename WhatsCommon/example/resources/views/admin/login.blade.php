<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title')</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="assets\vendors\iconfonts\mdi\css\materialdesignicons.min.css">
  <link rel="stylesheet" href="assets\vendors\iconfonts\puse-icons-feather\feather.css">
  <link rel="stylesheet" href="assets\vendors\css\vendor.bundle.base.css">
  <link rel="stylesheet" href="assets\vendors\css\vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="assets\css\style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="assets\images\logo.png" type="image/x-icon">
  <link rel="icon" href="assets/images/logo.png" type="image/x-icon">
</head>

<body style="background: url('assets/images/background.jpg');">
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
              <form action="" method="POST">
                <div class="form-group">
                  <label class="label">Username</label>
                  <div class="input-group">
                    <input type="text" name="user_email" value="" class="form-control" placeholder="Username">
                    <div class="input-group-append">
                      <span class="input-group-text"><i class="mdi mdi-check-circle-outline"></i></span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="label">Password</label>
                  <div class="input-group">
                    <input type="password" name="user_password" value="" class="form-control" placeholder="**************">
                    <div class="input-group-append">
                      <span class="input-group-text"><i class="mdi mdi-check-circle-outline"></i></span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <button class="btn btn-success submit-btn btn-block" type="submit">Login</button>
                </div>
                <div class="form-group d-flex justify-content-between">
                  <!-- <a href="forgot-password" class="text-small forgot-password text-black">Forgot Password</a> -->
                </div>
              </form>
            </div>
            <ul class="auth-footer">
              <li><a href="#">Conditions</a></li>
              <li><a href="#">Help</a></li>
              <li><a href="#">Terms</a></li>
            </ul>
            <p class="footer-text text-center">copyright © 2018 Bootstrapdash. All rights reserved.</p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="assets\vendors\js\vendor.bundle.base.js"></script>
  <script src="assets\vendors\js\vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="assets\js\off-canvas.js"></script>
  <script src="assets\js\hoverable-collapse.js"></script>
  <script src="assets\js\misc.js"></script>
  <script src="assets\js\settings.js"></script>
  <script src="assets\js\todolist.js"></script>
  <!-- endinject -->
</body>

</html>
