<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item "> 
      <a class="nav-link" href="<?php echo url('admin/dashboard') ?>">
        <img class="menu-icon" src="assets\images\menu_icons\Icon\7.png" alt="menu icon">
        <span class="menu-title">Dashboard</span>
      </a> 
    </li>


    <li class="nav-item"> 
      <a class="nav-link" href="<?php echo url('admin/pages') ?>">
        <img class="menu-icon" src="assets\images\menu_icons\Icon\9.png" alt="menu icon">
        <span class="menu-title">Pages</span>
      </a> 
    </li>


    <li class="nav-item "> 
      <a class="nav-link" href="<?php echo url('admin/users') ?>">
        <img class="menu-icon" src="assets\images\menu_icons\Icon\14.png" alt="menu icon">
        <span class="menu-title">Users</span>
      </a> 
    </li>
  </ul>
</nav>
<!-- partial -->
<div class="main-panel">