<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title')</title>
  <!-- plugins:css -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="assets\vendors\iconfonts\mdi\css\materialdesignicons.min.css">
  <link rel="stylesheet" href="assets\vendors\iconfonts\puse-icons-feather\feather.css">
  <link rel="stylesheet" href="assets\vendors\iconfonts\font-awesome\css\font-awesome.min.css">
  <link rel="stylesheet" href="assets\vendors\css\vendor.bundle.base.css">
  <link rel="stylesheet" href="assets\vendors\css\vendor.bundle.addons.css">
  <link rel="stylesheet" href="assets\css\datepicker.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="assets\css\style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
  <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
  <link rel="stylesheet" href="assets/vendors/sweet-alert/sweetalert.css">
  <link rel="stylesheet" href="assets/css/toastr.min.css">
  <link rel="stylesheet" href="assets/css/scrollBar.css">
  <!-- Latest compiled and minified CSS -->
 <!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"> -->

  <style>
.dt-buttons{
  margin-top: -7px;
}

button.dt-button, div.dt-button, a.dt-button {

margin-right: 5px !important;
padding: 2px 10px !important;
border:none !important;
border-radius: 0px !important;
background: none !important;
padding: 2px !important;
margin-top: 30px;
}

.dataTables_filter{
width: 25% !important;
margin-top: 27px !important;
}

.dataTables_filter input{
border: 1px solid #dedede;
}
.dataTables_length{
margin-top: 25px !important;
}
.sorting, .sorting_asc, .sorting_desc{
  background: none !important;
}
  .ajax_loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('assets/images/spinner.gif') center no-repeat transparent;
    background-size: 200px 200px;
  }
  #shrtbe li {
    background-color: honeydew;
  }
  #shrtbe li:nth-child(even) {
      background-color: gainsboro;
  }
.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:active, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: #999;
    background: white;
    border: none;
}.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 100% !important;
    /* border: 1px solid white; */
    border-bottom: 1px solid #b0afae;
}
.btn-secondary{
  background-color: #ebecee !important;
  border-color: #e0e4ee !important;
}
.bs-searchbox .form-control {
    border-radius: 20px !important;
}
.bootstrap-select .dropdown-menu li {
  font-weight: 800 !important;
  color: #e86338 !important;
}
.amount_p{
    border: 2px solid #e0bd90;
    padding: 10px 15px;
    color: #e86338;
    height: 40px;
    margin-top: 15px;
  }
  .spec_p{
    border: 1px solid #f3f5f6;
    padding: 15px 5px;
    margin-top: 5px;
    box-shadow: 3px 3px 3px burlywood;
  }
  .table_div{
    border: 1px solid #d7d7d7;
    padding: 10px;
  }
  .dropdown-menu{
    min-width: 100% !important;
    transform: unset !important;
  }
  .disabled{

  }
  .able{
    background: #c7eaa4 !important;
  }
  </style>
   <!-- plugins:js -->
<style>
/* width */
::-webkit-scrollbar {
  width: 5px;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey; 
  border-radius: 10px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #e86338; 
  border-radius: 10px;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #b30000; 
}
</style>
  <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <script src="assets\vendors\js\vendor.bundle.base.js"></script>
  <script src="assets\vendors\js\vendor.bundle.addons.js"></script>
  
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
  <script src="assets\js\bootstrap-datepicker.js"></script>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
   
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="assets\js\off-canvas.js"></script>
  <script src="assets\js\hoverable-collapse.js"></script>
  <script src="assets\js\misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- <script src="assets\js\data-table.js') ?>"></script> -->
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
  <script src="assets\js\admin\admin.js"></script>
  
  <script src="assets/js\dropify.js"></script>
  <script src="assets/vendors/sweet-alert/sweetalert-dev.js"></script>
  <!-- End custom js for this page-->
  <script src="assets/js/jquery-ui.min.js"></script>
  <!-- <script src="assets/js/perfect-scrollbar.js') ?>"></script> -->
  <script src="assets/js/scrollBar.js"></script>
  <!-- ck editor -->
  <script type="text/javascript" src="assets/vendors/ckeditor4/ckeditor.js"></script>
  <script type="text/javascript">

  function site_url(page_name=''){
    var site_url = '<?php echo url('/');  ?>';
    site_url = site_url+'/'+page_name;
    console.log(site_url);
    return site_url;
  }

  $(document).ready(function(e){
    $(".new-scrollbar").scrollBox();
    $('.multiple').select2();
    date_picker();
    $('.dropify').dropify();
    $('.select-picker').selectpicker();
  });

  function date_picker(){
    $(".no_previous_date").datepicker({ 
      format: "dd-mm-yyyy",
      minDate: '-0D',
      autoclose: true
    });
    $('.no_future_date').datepicker({
      format: "dd-mm-yyyy",
      endDate: '+0d',
      maxDate: '+0D',
      autoclose: true
    });
    $('.only-two-days').datepicker({
      format: "dd-mm-yyyy",
      endDate: '+0d',
      minDate: '-1D',
      maxDate: '+0D',
      autoclose: true
    });
    $(".from_tomorrow").datepicker({ 
      format: "dd-mm-yyyy",
      minDate: '+1D',
      autoclose: true
    });
  }
  </script>
  <script src="assets/js/toastr.min.js"></script>

  <script>
  $(document).ready(function () { 

    toastr.options = {
      "closeButton"       : false,
      "debug"             : false,
      "newestOnTop"       : true,
      "progressBar"       : true,
      "positionClass"     : "toast-bottom-right",
      "preventDuplicates" : false,
      "onclick"           : null,
      "showDuration"      : "300",
      "hideDuration"      : "1000",
      "timeOut"           : "5000",
      "extendedTimeOut"   : "1000",
      "showEasing"        : "swing",
      "hideEasing"        : "linear",
      "showMethod"        : "fadeIn",
      "hideMethod"        : "fadeOut"
    };
    
  }); 
  </script>
</head>

<body>
  <div id="loading" style="display:none;text-align: center;" class="ajax_loader"></div> 
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center " >
        <a class="" href="dashboard">
          <img src="assets/images/logo.png" style="max-width: 225px;margin-top: 5px;" alt="">
        </a>
      </div>
    
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center"><span></span></button>
        
        <ul class="navbar-nav navbar-nav-right">
          
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-account"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="UserDropdown">
              <a class="dropdown-item" href="javascript:void;" onclick="open_modal('update-profile');">
                Profile
              </a>
              <a href="javascript:void;" onclick="open_modal('change-password');" class="dropdown-item">
                Change Password
              </a>
              <a class="dropdown-item" href="logout">
                Sign Out
              </a>
            </div>
          </li>
          
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link" href="#" aria-expanded="false">
              <span class="mr-3">Hello </span>
            </a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>

      </div>
    </nav>

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      