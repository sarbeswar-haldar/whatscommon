@section('title', 'Edit Page')
@include('admin.inc.header')
@include('admin.inc.sidebar')
  <div class="content-wrapper">
    <div class="card">
      <div class="card-body">
        <form method="POST" class="form-horizontal admin_form" id="update-page">
          <div class="form-group" id="submit_status"></div>
          <input type="hidden" name="_page_id" value="{{$page->id}}">
          @csrf
          <div class="form-group">
            <label>Page Title<span class="text-danger">*</span></label>
            <input type="text" name="page_title" class="form-control" value="{{$page->page_title}}">
            <p class="text-danger" id="page_title_error"></p>
          </div>
          <div class="form-group">
            <label>Content<span class="text-danger">*</span></label>
            <textarea class="form-control ckeditor" name="page_content" rows="5">{{ $page->page_content }}</textarea>
            <p class="text-danger" id="page_content_error"></p>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
@include('admin.inc.footer')