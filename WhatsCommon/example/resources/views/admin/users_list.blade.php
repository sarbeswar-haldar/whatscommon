@section('title', 'Users')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Users</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        
        <!-- <div class="col-3 text-right">
          <a class="btn btn-secondary" href="javascript:void();" onclick="open_modal('add-company');"><i class="fa fa-plus-circle text-success"></i> Add Company</a>
        </div> -->
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="users-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Login Type</th>
                <th>Date Created</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
@if($users)
  @php
    $i = 1
  @endphp
  @foreach ($users as $user)
            <tr>
              <td>{{ $i++ }}</td>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->phone }}</td>
              <td class="text-center">
                @if($user->fb_id!='')
                  Facebook
                @elseif($user->g_id!='')
                  Google
                @else
                  Normal
                @endif
              </td>
              <td></td>
              <td>
                <!--<a class="custom_btn _edit" onclick="open_modal('edit-user','_user_id={{$user->id}}');">
                  <i class="fa fa-edit"></i>
                </a>-->
              </td>
            </tr>
  @endforeach
  @else
    <tr><td colspan="7" class="text-center"><b>No User Available</b></td></tr>
@endif

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#users-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2,3,4,5 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2,3,4,5 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2,3,4,5 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script>