<?php $__env->startSection('title', 'Dashboard'); ?>
<?php echo $__env->make('admin.inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.inc.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <div class="content-wrapper">
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center" id="line">
                <canvas  height="300" width="450"></canvas>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['line']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Day');
    data.addColumn('number', 'ABC');
    data.addColumn('number', 'DEF');

    data.addRows([
      ['Day 1', 0,0],
      ['Day 2', 1,3],
    ]);
    var options = {
      chart: {
        title: 'Last 7 Day Activity',
        subtitle: ''
      },
      axes: {
        x: {
          0: {side: 'top'}
        }
      },
    };

    var chart = new google.charts.Line(document.getElementById('line'));

    chart.draw(data, google.charts.Line.convertOptions(options));
  }
</script>
<?php echo $__env->make('admin.inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/worksamples/public_html/WhatsCommon/example/resources/views/admin/dashboard.blade.php ENDPATH**/ ?>