<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $date = date_create("now");
        DB::table('users')->insert([
    'name' => 'debasish',
    'email' => 'debasish@zabingo.com',
    'password' => Hash::make('tesla')
]);
        DB::table('userdetails')->insert([
    'name' => 'debaish',
    'email' => 'debasish@zabingo.com',
    'dob' => $date,
    'id' => '1',
    'phone' => '+91 8420489525',
    'country' => 'india',
    'state' => 'wb',
    'city' => 'kolkata',
    'province' => 'jnb',
    'street' => 'jnb',
    'zip' => '700147',
    'mi' => 'q',

]);
    }
}
