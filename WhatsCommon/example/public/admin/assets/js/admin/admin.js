$(document).ready(function () {

	$(document).on('keydown', '.numeric', function(e){-1!==$.inArray(e.keyCode,[8,9,46])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
	$(document).on('keydown', '.decimal', function(e){-1!==$.inArray(e.keyCode,[8,9,46,110])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
	$(document).on('submit' ,'.admin_form' , function(e){
		e.preventDefault();
		var submit_id = $(this).attr('id');
		var action    = site_url('ajax/'+submit_id);
		var submit_btn_content = $('#'+submit_id+' button[type="submit"]').html();

		$('#'+submit_id+' button[type="submit"]').html("<i class='fa fa-spinner fa-spin'></i> Loading");
		$('#'+submit_id+' button[type="submit"]').attr('disabled',true);
		$.ajax({
			type : "POST",
			data : new FormData(this),
			contentType: false,   
		    cache: false,             
		    processData:false, 
			url  : action,
			success : function(response){

				//console.log(response);return false;
				var obj = jQuery.parseJSON(response);
				$('html, body, modal').animate({ scrollTop: 0 }, 600);
				$('.dropify').dropify();	
				//Success Response Start
				if (obj.success) {
					$("#"+submit_id+" #submit_status").html(obj.success);

					$('#'+submit_id+' button[type="submit"]').attr("disabled", true);

					var no_error = obj.no_error;
				  	$.each(no_error, function(key, value){
				  		$("#"+submit_id+" #"+key+"_error").html('');
				  		if (obj.clear_value) {
				  	  		$("#"+submit_id+" input[name='"+key+"']").val('');	
				  		}
				  	});
				  	
				  	if(obj.close){
				  		$("#"+submit_id+"-modal").modal('hide');
				  	}
				  	$("#"+submit_id+" .show_file_name").html('');
				  	
					if (obj.redirect && obj.time) {
			  		window.setTimeout(function(){window.location.href = obj.redirect;}, obj.time);
				  	}
				  	else if(obj.redirect){
				  		$(location).attr("href", obj.redirect); 
				  	}
				  	else if(obj.time){
				  		window.setTimeout(function(){location.reload();}, obj.time);
				  	}
				  	else{
				  		if(obj.div_id){
				  			toastr.success(obj.success);
				  			if(obj.div_id == "select_company"){
				  				//console.log(obj.select_company);
				  				$('#select_company').html(obj.select_company);
				  				//$('#select_company').selectpicker('refresh');
				  				$('.select-picker').selectpicker('refresh');
				  			}else {
				  				$('#select_customer').html(obj.select_customer);
				  				$('#select_customer').selectpicker('refresh');
				  			}
				  			//console.log(obj.id_div);
				  		}else{
				  			location.reload();
				  		}
				  	}
				}
				//Success Response End

				//Not Success Response Start
				else if(obj.not_success){
					$('#'+submit_id+' button[type="submit"]').attr('disabled',false);
					$("#"+submit_id+" #submit_status").html(obj.not_success);

					var no_error = obj.no_error;
				  	  $.each(no_error, function(key, value){
					  	  $("#"+submit_id+" #"+key+"_error").html('');	
				  	});
				}
				//Not Success Response End

				//Validation Error Response Start
				else if (obj.error) {
					$('#'+submit_id+' button[type="submit"]').attr('disabled',false);
					$("#"+submit_id+" #submit_status").html(obj.common_error_msg);

					  $.each(obj.error, function(key, value){
					  	  if (obj.error[key]) {
					  	  	 $("#"+submit_id+" #"+key+"_error").html(obj.error[key]);
					  	  }
					  	  else{
					  	  	 $("#"+submit_id+" #"+key+"_error").html('');	
					  	  }
				  	  });

				  	 // alert(JSON.stringify(obj.error));
				}
				//Validation Error Response End

				//404 Error Response Start
				else if(obj.error_404){
					$('#'+submit_id+' button[type="submit"]').attr('disabled',false);
					$("#"+submit_id+" #submit_status").html(obj.error_404);

					var no_error = obj.no_error;
				  	  $.each(no_error, function(key, value){
					  	  $("#"+submit_id+" #"+key+"_error").html('');	
					  	  $('#'+submit_id+' input[name='+key+']').removeAttr('style');
					  	  $('#'+submit_id+' select[name='+key+']').removeAttr('style');
				  	});
				}
				//404 Error Response End

				$('#'+submit_id+' button[type="submit"]').html(submit_btn_content);
			}
		});
	});
   
    $("input[type='image']").click(function() {
		$("input[id='my_file']").click();
	}); 

	$(document).on('change','#product_category',function(e){
		var product_category = $(this).val();
		$.ajax({
			type : "POST",
			data : "product_category="+product_category,
			url  : site_url('ajax/get-by-category'),
			success : function(result){
				var obj = jQuery.parseJSON(result);

				if (obj.success) {
					if(obj.sub_categories){
						$('#product_sub_category').html(obj.sub_categories);
					}
				}
			}
		});
	});
});

function open_modal(link, data){
	$('#loading').show();

	var url = site_url('ajax/'+link+'-modal');

	$.ajax({
		type : "POST",
		data : data,
		url  : url,
		success:function(result){

			$('#loading').hide();

			var obj = jQuery.parseJSON(result);

			if (obj.success) {

				if ($("#"+link+"-modal").length!=0) {
					//$("#"+link+"-modal").remove();
					$("#"+link+"-modal").html('');
					//alert($("#"+link+"-modal").length);
				}

				//alert($("#"+link+"-modal").length);
					
				jQuery('body').prepend(obj.modal);

				//$('.selectpicker').selectpicker('refresh');
				$('.select-picker').selectpicker('refresh');
				$('.multiple').select2(/*{
				  theme: "classic"
				}*/);
				$('.dropify').dropify();
				/*if ($("#editor1").length) {
					CKEDITOR.replace( 'editor1', {
						customConfig: base_url('assets/js/ckeditor-config.js')
					});
				}*/

				  /*$(".only-two-days").datepicker({ 
				    format: "dd-mm-yyyy",
				    endDate: '+0d',
				    minDate: '-1D',
				    autoclose: true
				  });*/
				  date_picker();
				  //updatePickers();
				
			    $("#"+link+"-modal").modal('show');

			    /*if (obj.refresh) {
			  		dataTable.clear().draw();
			  	}*/
			}
			else if (obj.error_404) {
				toastr.error(obj.error_404);
			}
			else if (obj.error) {
				toastr.warning(obj.error);
			}
		}
	});
}




function execute(link, data){
   $('#loading').show();

	var url = site_url('ajax/'+link);

	$.ajax({
		type : "POST",
		data : data,
		url  : url,
		success:function(result){

			$('#loading').hide();

			var obj = jQuery.parseJSON(result);

			if (obj.success) {
				toastr.success(obj.success);

				/*if (obj.close) {
			  		$("#"+link+"-modal").modal('hide');

			  		dataTable.clear().draw();
				    
			  	}
			  	else*/ if (obj.redirect && obj.time) {
					window.setTimeout(function(){window.location.href = obj.redirect;}, obj.time);
			  	}
			  	else if(obj.redirect){
			  		$(location).attr("href", obj.redirect); 
			  	}
			  	else if(obj.time){
			  		window.setTimeout(function(){location.reload();}, obj.time);
			  	}else{
			  		location.reload();
			  	}
			}
			else if (obj.error_404) {
				toastr.error(obj.error_404);
			}
			else if (obj.error) {
				toastr.warning(obj.error);
				if(obj.time){
			  		//window.setTimeout(function(){location.reload();}, obj.time);
			  		location.reload();
			  	}
			}
		}
	});
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#profile_img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}