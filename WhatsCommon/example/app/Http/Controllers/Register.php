<?php

namespace App\Http\Controllers;

/*use Illuminate\Http\Request;*/
use Illuminate\Http\Request;
use App\Http\Controllers\DbHandler;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Userdetail;
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

class Register extends Controller
{
	function index(){
		return view("signup");
	}

    function stepOne(Request $request){
    	$validated = $request->validate([
	        'email' 	=> 	'required|email:filter|unique:users|max:255',
	        'username' 	=> 	'required|unique:users',
	        'last_name'	=>	'required',
	        'first_name'=>	'required',
	        'password'	=>	'required',
	        'cnf_pass' 	=>	'required|same:password'
	    ]);

	    $done = DB::table('users')->insert([
			        'name' 		=> $request->first_name.' '.$request->last_name,
			        'email' 	=> $request->email,
			        'password' 	=> Hash::make($request->password),
			        'username' 	=> $request->username,
			        'phone'   	=> $request->phone,
			        'mi'      	=> $request->mi
              	]);

	    if(!$done)
        {
            return response([
                      			'response_code'	=>	'201',
			                	'response_msg'	=> 	'User exists or invalid details',
			                	'reponse_body' 	=> 	"null"
			                ], 401);
        }
        else
        {
            $data = User::where('email', $request->email)->first();
            $request->session()->put('userdata', $data);
            return response([
                    			'response_code'	=>	'200',
                				'response_msg'	=> 	'User is created',
                  				'reponse_body' 	=> 	$data,
                  				'redirect'		=>	url('signup-details')
                  			], 200);

        }

	    /*print_r($validated);*/

	    /*if ($validated->fails()) {
	    	return response([
                          'response_code'=>'201',
                      'response_msg'=> 'Complete fields correctly',
                      'reponse_body' => $validated
                      ], 401);
	    }*/
    }

    function signupDetails(Request $request){
    	if ($request->session()->has('userdata')) {
    		return view("signup_details");
    	}else{
    		return redirect('signup');
    	}
    }

    function stepTwo(Request $request){
    	$validated = $request->validate([
	        'email' 	=> 	'required|email:filter|unique:userdetails|max:255',
	        'phone'		=>	'required|numeric|digits:10',
	        'dob' 		=> 	'required|date',
	        /*'last_name'	=>	'required',
	        'first_name'=>	'required',
	        'password'	=>	'required',
	        'cnf_pass' 	=>	'required|same:password'*/
	    ]);
    	$userdata = $request->session()->get('userdata');
	    $done =  DB::table('userdetails')->insert([
													'name' 		=> 	$userdata->name,
												    'email' 	=> 	$request->email,
												    'dob' 		=> 	$request->dob,
												    'id' 		=> 	$userdata->id,
												    'phone' 	=> 	$request->phone,
												    'country' 	=> 	$request->country,
												    'state' 	=> 	$request->state,
												    'city' 		=> 	$request->city,
												    'province' 	=> 	$request->province,
												    'street' 	=> 	$request->street,
												    'zip' 		=> 	$request->zip,
												    'mi' 		=> 	$userdata->mi
										        ]);

        if(!$done){
            return response([
                      			'response_code'=>'201',
                  				'response_msg'=> 'User exists or invalid details',
                  				'reponse_body' => "null"
                  			], 401);
        }
        else{
        	$totalData = Userdetail::where('email',$request->email) -> first();
        	$request->session()->forget('userdata');
            return response([
                    			'response_code'	=>	'200',
                  				'response_msg'	=> 	'User details inserted',
                  				'reponse_body' 	=> 	$totalData,
                  				'redirect'		=>	url('/')
                  			], 200);
        }
    }

    function checkEmail(Request $request){
    	$validated = $request->validate([
	        'email' 	=> 	'required|email:filter',
	    ]);
    	$dat = DB::table("users")->select('id')->where('email',$request->email)->first();
	    if($dat)
        {
        	$rand = rand(100000,900000);
           	$data = [
          				'subject' 	=> 	'OTP GENERATED',
          				'name' 		=> 	'WhatsCommon',
          				'email' 	=> 	$request->email,
          				'content' 	=> 	$rand
        			];
        	User::where('email',$request->email)->update(['remember_token'=>$rand]);
        	Mail::send('otppage', $data, function($message) use ($data) {
          		$message->to($data['email'])->subject($data['subject']);
        	});
            // Mail::to($request->email)->send(new OrderShipped());
            $request->session()->put('id', $dat->id);
            return response([
                				'response_code'	=>	'200',
                				'response_msg'	=> 	'an otp has been sent',
                				'redirect'		=>	url('reset-password'),
                				'reponse_body' 	=>	$dat],200);
        }

        return response([
                        	'response_code'	=>	'203',
                     		'response_msg'	=> 	'There are no such record exists with this email',
                     		'reponse_body' 	=> 	"null"
                     	], 401);
    }

    function setPassword(Request $request){
    	$validated = $request->validate([
	        'otp' 				=> 	'required',
	        'password'			=>	'required',
	        'confirm_password' 	=>	'required|same:password'
	    ]);

	    if ($request->session()->has('id')) {
	    	$id = $request->session()->get('id');
	    	$user_data = DB::table("users")->where('id',$id)->first();

	    	if($user_data){
	    		if($user_data->remember_token==$request->otp){
	    			User::where('id',$id)->update([
        											'password'			=>	Hash::make($request->password),
        											'remember_token'	=>	null
        										]);
	    			return response([
	    								'response_code'	=>	'200',
                         				'response_msg'	=> 	'password set successfully',
                         				'reponse_body' 	=> 	'null',
                         				'redirect'		=>	url('login')
                         			],200);
	    		}else{
	    			return response([
                        	'response_code'	=>	'203',
                     		'response_msg'	=> 	'Invalid OTP Entered',
                     		'reponse_body' 	=> 	"null"
                     	], 401);
	    		}
	    	}
	    }

	    return response([
                        	'response_code'	=>	'203',
                     		'response_msg'	=> 	'Invalid Request',
                     		'reponse_body' 	=> 	"null"
                     	], 401);
    }
}
