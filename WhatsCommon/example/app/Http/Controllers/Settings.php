<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Userdetail;
use App\Models\BankDetail;
use App\Models\NotificationSetting;
use Validator;

class Settings extends Controller
{
    function findBankDetails(Request $request){
    	$rules =[
			        'user_id' 	=> 	'required',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$banks = BankDetail::where('bank_user_id',$request->user_id)->get();

			return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Financial settings fetched successfully',
                         		'reponse_body' 	=> 	$banks
                        	], 200);
		}
    }

    function addBankDetails(Request $request){

    	$rules =[
			        'type' 		=> 	'required',
			        'user_id' 	=> 	'required',
			    ];
		if($request->type=='card'){

			$rules['card_number'] 		=	'required';
			$rules['card_expiry_date'] 	=	'required';
			$rules['card_cvv'] 			=	'required';
			$rules['card_holder_name'] 	=	'required';

		}else if($request->type=='paypal'){

			$rules['paypal_email'] 		=	'required';
			$rules['paypal_password'] 	=	'required';

		}else if($request->type=='bank'){

			$rules['card_holder_name'] 	=	'required';
			$rules['bank_name'] 		=	'required';
			$rules['account_number'] 	=	'required';
			$rules['routing_number'] 	=	'required';

		}
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{

			if($request->type=='card'){
				$bank_details = [
									'bank_type'				=>	'card',
									'bank_user_id'			=>	$request->user_id,
									'bank_is_default'		=>	($request->is_default && $request->is_default==1) ? 1 : 0,
									'bank_ac_or_card_no'	=>	$request->card_number,
									'bank_card_holder_name'	=>	$request->card_holder_name,
									'bank_card_expiry'		=>	$request->card_expiry_date,
									'bank_card_cvv'			=>	$request->card_cvv
								];
			}else if($request->type=='paypal'){
				$bank_details = [
									'bank_type'				=>	'paypal',
									'bank_user_id'			=>	$request->user_id,
									'bank_is_default'		=>	($request->is_default && $request->is_default==1) ? 1 : 0,
									'bank_email'			=>	$request->paypal_email,
									'bank_password'			=>	$request->paypal_password
								];
			}else if($request->type=='bank'){
				$bank_details = [
									'bank_type'				=>	'paypal',
									'bank_user_id'			=>	$request->user_id,
									'bank_is_default'		=>	($request->is_default && $request->is_default==1) ? 1 : 0,
									'bank_card_holder_name'	=>	$request->card_holder_name,
									'bank_ac_or_card_no'	=>	$request->account_number,
									'bank_name'				=>	$request->bank_name,
									'bank_routing_no'		=>	$request->routing_number
								];
			}

			if($request->is_default && $request->is_default==1){
				BankDetail::where('bank_user_id',$request->user_id)->update(['bank_is_default'=>0]);
			}

			BankDetail::insert($bank_details);
			$banks = BankDetail::where('bank_user_id',$request->user_id)->get();

			return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Financial setting updated successfully',
                         		'reponse_body' 	=> 	$banks
                        	], 200);
		}
    	
    }

    function addDonation(Request $request){

    	$rules =[
			        'currency_type' 		=> 	'required',
			        'user_id' 				=> 	'required',
			        'donation_amount'		=>	'required',
			        'bank_id'				=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$done = DB::table('donations')->insert([
				        	'donation_currency'	=> $request->currency_type,
				        	'donation_amount' 	=> $request->donation_amount,
				        	'donation_user_id' 	=> $request->user_id,
				        	'donation_bank_id' 	=> $request->bank_id
              			]);

            if(!$done)
            {
                return response([
                        		'response_code'	=>	'201',
                      			'response_msg'	=> 	'invalid details',
                      			'reponse_body' 	=> 	"null"
                      		], 401);
            }
            else
            {
                return response([
                          		'response_code'=>'200',
                    			'response_msg'=> 'Donation is created',
                      			'reponse_body' => "null"
                      		], 200);
            }
		}
    }

    function getNotificationSetting(Request $request){
    	$rules =[
			        'user_id' 	=> 	'required',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$notifications = NotificationSetting::where('ns_user_id',$request->user_id)->first();

			return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Notification settings fetched successfully',
                         		'reponse_body' 	=> 	$notifications
                        	], 200);
		}
    }

    function setNotificationSetting(Request $request){
    	$rules =[
			        'message_setting' 		=> 	'required',
			        'user_id' 				=> 	'required',
			        'share_setting'			=>	'required',
			        'new_connection_setting'=>	'required',
			        'new_match_setting'		=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$notifications = NotificationSetting::where('ns_user_id',$request->user_id)->first();

			$message_setting = ($request->message_setting && $request->message_setting==1) ? 1 : 0;
			$share_setting = ($request->share_setting && $request->share_setting==1) ? 1 : 0;
			$new_connection_setting = ($request->new_connection_setting && $request->new_connection_setting==1) ? 1 : 0;
			$new_match_setting = ($request->new_match_setting && $request->new_match_setting==1) ? 1 : 0;

			if($notifications){
				NotificationSetting::where('ns_user_id',$request->user_id)->update([
												'ns_message'		=>	$message_setting,
												'ns_shares'			=>	$share_setting,
												'ns_new_connection'	=>	$new_connection_setting,
												'ns_new_match'		=>	$new_match_setting
											]);
			}else{

				NotificationSetting::insert([
												'ns_user_id'		=>	$request->user_id,
												'ns_message'		=>	$message_setting,
												'ns_shares'			=>	$share_setting,
												'ns_new_connection'	=>	$new_connection_setting,
												'ns_new_match'		=>	$new_match_setting
											]);
			}
			$notifications = NotificationSetting::where('ns_user_id',$request->user_id)->first();

            return response([
                          		'response_code'=>'200',
                    			'response_msg'=> 'Notification settings updated successfully',
                      			'reponse_body' => ['notification_settings' => $notifications]
                      		], 200);
		}
    }

    function getPremiumPlans(Request $request){
    	$plans = DB::table('premium_plans')->get();
    	return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Premium Plans Fetched Successfully',
                  			'reponse_body' => ['premium_plans' => $plans]
                  		], 200);
    }

    function choosePlan(Request $request){
    	$rules =[
			        'plan_id' 				=> 	'required',
			        'user_id' 				=> 	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$plan_data = DB::table('premium_plans')->find($request->plan_id);
			if($plan_data){
				$chosen_plan = 	[
									'cp_user_id'	=>	$request->user_id,
									'cp_plan_id'	=>	$request->plan_id
								];
				if($plan_data->plan_type=='monthly'){

					$expiry_date = date('Y-m-d', strtotime('+1 month', strtotime(date('Y-m-d'))));
					$chosen_plan['cp_expiry_date']	= $expiry_date;

				}else if($plan_data->plan_type=='annually'){
					$expiry_date = date('Y-m-d', strtotime('+12 month', date('Y-m-d')));
					$chosen_plan['cp_expiry_date']	= $expiry_date;
				}

				DB::table('chosen_plans')->insert($chosen_plan);
				return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Plan Chosen Successfully',
                  				'reponse_body' 	=> 	"null"
                  		], 200);
			}else{
				return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Invalid Details Supplied',
                         		'reponse_body' 	=> 	"null"
                        	], 401);
			}
		}
    }

    function getCountries(Request $request){
    	$countries = DB::table('countries')->get();
    	return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Countries Fetched Successfully',
                  			'reponse_body' => ['countries' => $countries]
                  		], 200);
    }

    function getProvince(Request $request){

    	if($request->_country_id){
    		$provinces = DB::table('provinces')->where('province_country',$request->_country_id)->get();
    	}else{
    		$provinces = DB::table('provinces')->get();
    	}

    	return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Provinces Fetched Successfully',
                  			'reponse_body' => ['provinces' => $provinces]
                  		], 200);
    }

    function getCity(Request $request){

    	if($request->_province_id){
    		$cities = DB::table('cities')->where('city_province',$request->_province_id)->get();
    	}else{
    		$cities = DB::table('cities')->get();
    	}

    	return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Cities Fetched Successfully',
                  			'reponse_body' => ['cities' => $cities]
                  		], 200);
    }
}
