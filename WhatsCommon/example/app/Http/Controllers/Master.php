<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;

class Master extends Controller
{
    function GenderList(Request $request){
    	$genders = DB::table('genders')->get();

    	return View::make("admin/genders_list")->with(['genders' => $genders]);
    }

    function RelationList(Request $request){
    	$relations = DB::table('relations')->get();

    	return View::make("admin/relations_list")->with(['relations' => $relations]);
    }

    function HomeStyleList(Request $request){
    	$home_styles = DB::table('home_styles')->get();

    	return View::make("admin/home_styles_list")->with(['home_styles' => $home_styles]);
    }

    function HomeTypeList(Request $request){
    	$home_types = DB::table('home_types')->get();

    	return View::make("admin/home_types_list")->with(['home_types' => $home_types]);
    }

    function MaritalStatusList(Request $request){
    	$marital_status = DB::table('marital_status')->get();

    	return View::make("admin/marital_status_list")->with(['marital_status' => $marital_status]);
    }

    function KeywordsList(Request $request){
    	$keywords = DB::table('keywords')->get();

    	return View::make("admin/keywords_list")->with(['keywords' => $keywords]);
    }

    function VehicleTypeList(Request $request){
    	$vehicle_types = DB::table('vehicle_types')->get();

    	return View::make("admin/vehicle_types_list")->with(['vehicle_types' => $vehicle_types]);
    }

    function CountryList(Request $request){
    	$countries = DB::table('countries')->get();

    	return View::make("admin/countries_list")->with(['countries' => $countries]);
    }

    function ProvincesList(Request $request){
    	if($request->_country_id){
    		$provinces = DB::table('provinces')->join('countries', 'countries._country_id', '=', 'provinces.province_country')->where('province_country',$request->_country_id)->get();
    	}else{
    		$provinces = DB::table('provinces')->join('countries', 'countries._country_id', '=', 'provinces.province_country')->get();
    	}

    	return View::make("admin/provinces_list")->with(['provinces' => $provinces]);
    }

    function CitiesList(Request $request){
    	$cities = DB::table('cities')->get();

    	return View::make("admin/cities_list")->with(['cities' => $cities]);
    }

    function StatesList(Request $request){
        $cities = DB::table('cities')->get();

        return View::make("admin/cities_list")->with(['cities' => $cities]);
    }
}
