<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Page;
use View;

class Pages extends Controller
{
    function PagesList(Request $request){
    	$pages = Page::all();

    	return View::make("admin/pages_list")->with(['pages' => $pages]);
    }
}
