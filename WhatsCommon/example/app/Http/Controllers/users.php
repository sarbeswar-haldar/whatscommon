<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DbHandler;
use Illuminate\Support\Facades\DB;
//use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Feedback;
use App\Models\Userdetail;
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use View;
//use App\Http\Views\Response;

class users extends Controller
{
    //
    function printMsg($data)
    {
        echo $data;
    }
    
    function CheckRequestHeader(Request $request)
    {
        if($request->hasHeader('Api-Auth-Key'))
        {
            if($request->header('Api-Auth-Key') == '#whts_cmn_8080')
            {
                return true;
            }
            else
                return false;
        }
        return false;
    }
    function gesture()
    {
    }
    function getUsers(Request $request)
    {
        $is_user_name = false;
            $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
            trim($request->email,' ');
            $user= User::where('email', $request->email)->first();
            
            if(!$user)
            {
                $user = User::where('username', $request->email)->first();
                $is_user_name = true;
            }
                
            // print_r($data);
            if (!$user || !Hash::check($request->password, $user->password)) {
                return response([
                    'response_code'=>'201',
                'response_msg'=> 'Invalid details',
                'reponse_body' => "null"
                ], 401);
            }

            //$token = $user->createToken('my-app-token')->plainTextToken;
           $data = Userdetail::where('email', $request->email)->first();
          
           if(!$is_user_name){
            
            $data_user = User::where('email',$request->email)->first();
            if($data && $data_user)
            $newData = array_merge($data->toArray(),$data_user->toArray());

            
           }
           else
           {
               
            $data_user = User::where('username',$request->email)->first();
            if($data && $data_user)
            $newData = array_merge($data->toArray(),$data_user->toArray());
           }

           
            //$data_user['profile']

           
            
            if(!$data)
            {
                return response([
                    'response_code'=>'202',
                'response_msg'=> 'details are incomplete',
                'reponse_body' => $data_user
                ], 202);
            }
            $response = [
                'response_code'=>'200',
                'response_msg'=> 'login successful',
                'reponse_body' => $newData
                
                
            ];

            return response($response, 200);


    }

    function updateImage(Request $request)
    {
             $CheckHeader =$this->CheckRequestHeader($request);
            
             if(!$CheckHeader)
             {
                 return response([
                     'response_code'=>'401',
                 'response_msg'=> 'header missing or invalid',
                 'reponse_body' => "null"
                 ], 401);
             }
             $checkFirst = User::where('username',$request->username)->first();

             if(!$checkFirst)
             {
                return response([
                     'response_code'=>'401',
                 'response_msg'=> 'Invalid username',
                 'reponse_body' => "null"
                 ], 401);
             }

        $updateImage = $this->getProfImage($request);
        //
        
        if($updateImage)
        {
            $profUrl = url('/profilePictures/'.$request->username.'.png');
            $dbUpdate = User::where('username',$request->username)->update(["profile_photo_path"=>$profUrl]);
            if(!$dbUpdate)
            {
                 return response([
                     'response_code'=>'401',
                 'response_msg'=> 'update to db failed',
                 'reponse_body' => "null"
                 ], 401);
            }

            return response([
            'response_code'=>'200',
                      'response_msg'=> 'profile picture updated',
                      'reponse_body' => $profUrl
            ]);
        }
        return response([
                     'response_code'=>'401',
                 'response_msg'=> 'image update failed',
                 'reponse_body' => "null"
                 ], 401);
    }

    function getProfImage(Request $request)
    {
        $base64Encoded = $request->imageencoded;
        if(!empty($base64Encoded))
            {
                $usrName = $request->username;
              $poc =  \Image::make($base64Encoded)->save(public_path('profilePictures/'.$usrName.'.png'));
              if($poc)
              {
               return true;
               }
               return false;
            }
            
            return false;
    }
    function registerUser(Request $request)
    {
        // if(count($request->toArray()) <3 || count($request->toArray()) > 3)
        // {
        //     return response([
        //                  'response_code'=>'201',
        //              'response_msg'=> 'Invalid number of params',
        //              'reponse_body' => "null"
        //              ], 401);
        // }
        
       // die("first name is :".$request->firstName);
       
        
         $CheckHeader =$this->CheckRequestHeader($request);
            
             if(!$CheckHeader)
             {
                 return response([
                     'response_code'=>'401',
                 'response_msg'=> 'header missing or invalid',
                 'reponse_body' => "null"
                 ], 401);
             }

        $Existing = User::where('email',$request->email)->first();
        // return response([
        //             'response_code'=>'200',
        //         'response_msg'=> $request->fname,
        //         'reponse_body' => "null"
        //         ], 200);

        if($Existing)
        {
             return response([
                          'response_code'=>'201',
                      'response_msg'=> 'email exists already',
                      'reponse_body' => "null"
                      ]);
        }
        $Existing = User::where('username',$request->username)->first();
        if($Existing)
        {
            return response([
                          'response_code'=>'201',
                      'response_msg'=> 'username exists already',
                      'reponse_body' => "null"
                      ], 401);
        }
        
        //Saving profile image if exists
        $imageSavedStat = $this->getProfImage($request);

        $profUrl = url('/profilePictures/'.$request->username.'.png');

        if(!$imageSavedStat)
        {
            $profUrl = url('/profilePictures/default.png');
        }
        
            //die("first name is :".$request->name);
            $dat = explode('@',$request->name);
            
            $done =  User::insert([
          'firstName' => $dat[0],
          'lastName' => $dat[1],
          'email' => $request->email,
          'password' => Hash::make($request->password),
          'username' => $request->username,
          'phone'   => $request->phone,
          'mi'      => $request->mi,
          'profile_photo_path' => $profUrl
              ]);

            if(!$done)
            {
                return response([
                          'response_code'=>'201',
                      'response_msg'=> 'User exists or invalid details',
                      'reponse_body' => "null"
                      ], 401);
            }
            else
            {
                $data = User::where('email',$request->email) -> first();
               // $data['profile_photo_path'] = url('/profilePictures/'.$request->username.'.png');
                return response([
                          'response_code'=>'200',
                      'response_msg'=> 'User is created',
                      'reponse_body' => $data
                      ], 200);
            }


            return response([
                          'response_code'=>'201',
                      'response_msg'=> 'User exists or invalid details',
                      'reponse_body' => "null"
                      ], 401);

    }

    function registerDetails(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        $Existing = Userdetail::where('email',$request->email)->first();

        if($Existing)
        {
            return response([
                         'response_code'=>'201',
                     'response_msg'=> 'User exists already',
                     'reponse_body' => "null"
                     ], 401);
        }

        $dat = explode('@',$request->name);
        $done =  DB::table('userdetails')->insert([
    'firstName' =>$dat[0],
    'lastName' => $dat[1],
    'email' => $request->email,
    'dob' => $request->dob,
    'id' => $request->id,
    'phone' => $request->phone,
    'country' => $request->country,
    'state' => $request->state,
    'city' => $request->city,
    'province' => $request->province,
    'street' => $request->street,
    'zip' => $request->zip,
    'mi' => $request->mi

            ]);

        if(!$done)
            {
                return response([
                          'response_code'=>'201',
                      'response_msg'=> 'User exists or invalid details',
                      'reponse_body' => "null"
                      ], 401);
            }
            else
            {
            $totalData = Userdetail::where('email',$request->email) -> first();
            $loginData = User::where('email',$request->email)->first();
            $newData = array_merge($totalData->toArray(),$loginData->toArray());
                return response([
                          'response_code'=>'200',
                      'response_msg'=> 'User details inserted',
                      'reponse_body' => $newData
                      ], 200);
            }



            return response([
                          'response_code'=>'201',
                      'response_msg'=> 'User exists or invalid details',
                      'reponse_body' => "null"
                      ], 401);

    }
    function checkVarifiedEmail(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }

        if(empty($request->email))
        {
            return response([
                          'response_code'=>'201',
                      'response_msg'=> 'Field is blank',
                      'reponse_body' => "null"
                      ], 401);
        }

        $dat = DB::table("users")->select('id')->where('email',$request->email)->first();

        if($dat)
        { $rand = rand(100000,900000);
            $data = [
          'subject' => 'OTP GENERATED',
          'name' => 'Whats common',
          'email' => $request->email,
          'content' => $rand
        ];
        User::where('email',$request->email)->update(['remember_token'=>$rand]);
        Mail::send('otppage', $data, function($message) use ($data) {
          $message->to($data['email'])
          ->subject($data['subject']);
          
        });
            // Mail::to($request->email)->send(new OrderShipped());
            return response([
                'response_code'=>'200',
                'response_msg'=> 'an otp has been sent',
                'reponse_body' =>$dat],200);
        }

        return response([
                         'response_code'=>'203',
                     'response_msg'=> 'There are no such record exists with this email',
                     'reponse_body' => "null"
                     ], 401);
    }
    function updateProfile(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        try{
            $request->validate(
            [
                'email'=>'required',
                'id'=>'required',
                'name'=>'required',
                'dob'=>'required',
                'phone'=>'required',
                'country'=>'required',
                'state'=>'required',
                'city'=>'required',
                'province'=>'required',
                'street'=>'required',
                'zip'=>'required',
                'mi'=>'required',
            ]
            );
        }
        catch(Exception $exception)
        {
            return response([
                'response_code'=>'204',
                         'response_msg'=> 'Not enough details are provided',
                         'reponse_body' => "null"
                         ], 401);
        }


        $totalData = Userdetail::where('id',$request->id) -> first();

        if($totalData)
        {
            DB::table('userdetails')->where('id',$request->id)->update([
                'email'=>$request->email,
                'name'=>$request->name,
                'dob'=>$request->dob,
                'phone'=>$request->phone,
                'country'=>$request->country,
                'state'=>$request->state,
                'city'=>$request->city,
                'province'=>$request->province,
                'street'=>$request->street,
                'zip'=>$request->zip,
                'mi'=>$request->mi,
                ]);
                User::where('id',$request->id)->update(['email'=>$request->email]);
            $totalData = Userdetail::where('id',$request->id) -> first();
            return response([
                             'response_code'=>'200',
                         'response_msg'=> 'data updated successfully',
                         'reponse_body' => $totalData
                         ], 200);
        }
        else
        {
            return response([
                             'response_code'=>'203',
                         'response_msg'=> 'There are no such record exists with this email',
                         'reponse_body' => "null"
                         ], 401);
        }
    }

    function ChnagePassword(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        $request->validate([
            'old_password'=>'required',
            'new_password'=>'required',
            'id'          =>'required'
            ]);

        //DB::table('users')->select('password')->where('id',$request->id)
          $pass =  User::where('id',$request->id)->first();
        //return response([
        //    'body'=>$pass
        //    ]);

        if($pass)
        {
            if(Hash::check($request->old_password,$pass->password))
            {
                User::where('id',$request->id)->update([
                    'password'=>Hash::make($request->new_password)
                    ]);
                $pass =  User::where('id',$request->id)->first();
                return response([
                    'response_code'=>'200',
                         'response_msg'=> 'password updated successfully',
                         'reponse_body' => $pass
                         ], 200);
               // $this->getUsers($request);
            }
            return response([
                    'response_code'=>'203',
                         'response_msg'=> 'Current password is wrong',
                         'reponse_body' => 'null'
                         ], 401);
        }
        return response([
                    'response_code'=>'203',
                         'response_msg'=> 'no such record',
                         'reponse_body' => 'null'
                         ], 401);
    }
    function verifyOtp(Request $request)
    {
        //$CheckHeader =$this->CheckRequestHeader($request);
            
        //    if(!$CheckHeader)
        //    {
        //        return response([
        //            'response_code'=>'401',
        //        'response_msg'=> 'header missing or invalid',
        //        'reponse_body' => "null"
        //        ], 401);
        //    }
        //$request->validate([
        //    'otp'=>'required',
        //    'id'=>'required'
        //    ]);
            
            $dat = User::where('id',$request->id)->first();
            if($dat)
            {
                if($request->otp == $dat->remember_token)
                {
                    User::where('id',$request->id)->update(['remember_token'=>null]);
                    return true;
                }
                return false;
            }
            return false;
            
    }
    function newPassword(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }

            $validateOtp = $this->verifyOtp($request);

            if(!$validateOtp)
            {
                 return response(['response_code'=>'401',
                         'response_msg'=> 'Invalid otp or id',
                         'reponse_body' => 'null'],401);
            }
        if(!empty($request->new_password) && !empty($request->id))
        {
            User::where('id',$request->id)->update(['password'=>Hash::make($request->new_password)]);
            return response(['response_code'=>'200',
                         'response_msg'=> 'new password is set',
                         'reponse_body' => 'null'],200);
        }
    }

    function UsersList(Request $request){
      $users = DB::table('users')->get();
      return View::make("admin/users_list")->with(['title' => 'Users','users' => $users]);
    }
    function SetUserLanguage(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        if(empty($request->id) || empty($request->lang))
        {
            return response(['response_code'=>'401',
                         'response_msg'=> 'proper form keys are not posted',
                         'reponse_body' => 'null'],401);
        }
        
        User::where('id',$request->id)->update(['userlang'=>$request->lang]);
        return response(['response_code'=>'200',
                         'response_msg'=> 'user language is set',
                         'reponse_body' => 'null'],200);
    }
    function submitFeedback(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        if(empty($request->id) || empty($request->stars) || empty($request->comment) || empty($request->type))
        {
            return response(['response_code'=>'401',
                         'response_msg'=> 'proper form keys are not posted',
                         'reponse_body' => 'null'],401);
        }
        
        $check = User::where('id',$request->id)->first();
        if(!$check)
        {
            return response(['response_code'=>'401',
                         'response_msg'=> 'id does not exist in database',
                         'reponse_body' => 'null'],401);
        }
      $data =   Feedback::where('id',$request->id)->first();
      if($data)
      {
          $override = Feedback::where('id',$request->id)->update(['stars'=>$request->stars,'comment'=>$request->comment,'type'=>$request->type]);
          return response(['response_code'=>'200',
                         'response_msg'=> 'feedback is updated',
                         'reponse_body' => 'null'],200);
      }
      else
      {
          Feedback::insert(['id'=>$request->id,'stars'=>$request->stars,'comment'=>$request->comment,'type'=>$request->type]);
          return response(['response_code'=>'200',
                         'response_msg'=> 'feedback is submitted',
                         'reponse_body' => 'null'],200);
      }
    }
    
    function ConnectContact(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        $request->validate([
          'id'=>'required',
          'stat'=>'required|max:1'
        ]);
        
        $dat = User::where('id',$request->id)->first();
        if($dat)
        {
            User::where('id',$request->id)->update(['connect'=>$request->stat]);
             return response(['response_code'=>'200',
                         'response_msg'=> 'connect updated',
                         'reponse_body' => 'null'],200);
        }
         return response(['response_code'=>'401',
                         'response_msg'=> 'id does not exists',
                         'reponse_body' => 'null'],401);
    }
    
    function AllowDiscovery(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        $request->validate([
          'id'=>'required',
          'stat'=>'required|max:1'
        ]);
        
        $dat = User::where('id',$request->id)->first();
        if($dat)
        {
            User::where('id',$request->id)->update(['discovery'=>$request->stat]);
             return response(['response_code'=>'200',
                         'response_msg'=> 'discovery updated',
                         'reponse_body' => 'null'],200);
        }
         return response(['response_code'=>'401',
                         'response_msg'=> 'id does not exists',
                         'reponse_body' => 'null'],401);
    }
    
    function getUserKeywords(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        $request->validate([
          'id'=>'required',
          'keywords'=>'required'
        ]);
        
        $splittedKeys = explode(",",$request->keywords);
        
        $dat = DB::table('keywords')->select('userkeys')->get()->toarray();
        $temp='';
        
        foreach($dat as $key)
        {
            $temp .= $key->userkeys.',';
        }
        $temp = explode(',',$temp);
       
        if($dat)
        { $i = 0;
        foreach($splittedKeys as $keys)
        {
            
            if(!in_array($keys,$temp))
            {
                //die('not in array '.$keys.'in '.$temp[$i]);
                DB::table('keywords')->insert(['userkeys'=>$keys]);
                  
            }
            
            $keyid = DB::table('keywords')->select('keyid')->where('userkeys',$keys)->first();
            $temp1 = '';
            
            $userKeys = DB::table('keywordslinks')->select('keyid')->where('id',$request->id)->get();
            
            foreach($userKeys as $key)
            {
                $temp1.= $key->keyid.',';
            }
            $temp1 = explode(',',$temp1);
            //die($keyid->keyid);
            // return response(['response_code'=>'200',
            //              'response_msg'=> $userKeys,
            //              'reponse_body' => 'null'],200);
            if($keyid)
            {
                if(!in_array($keyid->keyid,$temp1))   
                DB::table('keywordslinks')->insert(['id'=>$request->id,'keyid'=>$keyid->keyid]);
                
            }
            $i++;
        }
        return response(['response_code'=>'200',
                         'response_msg'=> 'keywords updated successfully',
                         'reponse_body' => 'null'],200);
        }
        return response(['response_code'=>'203',
                         'response_msg'=> 'databse error',
                         'reponse_body' => 'null'],200);
    }
    
}
