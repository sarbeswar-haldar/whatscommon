<?php

namespace App\Http\Controllers;
use App\Models\Appinformation;
use Illuminate\Support\Facades\DB;
use App\Models\Contactedperson;
use App\Models\User;
use App\Models\Feedbacktype;
use App\Models\Language;
//use Illuminate\Http\Request;

use Illuminate\Http\Request;

class AppInformations extends Controller
{
    function CheckRequestHeader(Request $request)
    {
        if($request->hasHeader('Api-Auth-Key'))
        {
            if($request->header('Api-Auth-Key') == '#whts_cmn_8080')
            {
                return true;
            }
            else
                return false;
        }
        return false;
    }
   function GetAppInfo(Request $request)
   {
      $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
       $data =  Appinformation::select('*')->first();
       $comp = DB::table('companyinfo')->select('*')->first();
       $feed = Feedbacktype::select('*')->get();
       $lang = Language::select('*')->get();
       $data['companyinfo'] = $comp;
       $data['feedbacktype'] = $feed;
       $data['getlang'] = $lang;
      if($data)
      {
          return response([
              'response_code'=>'200',
            'response_msg'=> 'got informations',
            'reponse_body' => $data,
              ],200);
      }
      else
      {
          return response([
                'response_code'=>'201',
            'response_msg'=> 'information error',
            'reponse_body' => "null"
            ], 401);
      }
   }
   function ContactUs(Request $request)
   {
       $CheckHeader =$this->CheckRequestHeader($request);
            
            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
       $dat = User::where('id',$request->id)->first();
       if($dat)
       {
           Contactedperson::insert(['id'=>$request->id,'name'=>$request->name,'message'=>$request->message,'email'=>$request->email]);
            return response([
              'response_code'=>'200',
            'response_msg'=> 'you message has been submitted',
            'reponse_body' => "null",
              ],200);
       }
        return response([
                'response_code'=>'201',
                'response_msg'=> 'information error',
                'reponse_body' => "null"
                ], 401);
   }
}
