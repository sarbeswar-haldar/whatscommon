<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Userdetail;
use App\Models\BankDetail;
use App\Models\NotificationSetting;
use Validator;

class LifeEvent extends Controller
{
    function getSubcategories(Request $request){
    	$rules =[
			        'type_id' 	=> 	'required',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$what = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', $request->type_id)->get();
			return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'What Fetched Successfully',
                  				'reponse_body' 	=> 	["what" => $what]
                  			], 200);
		}
    }

    function getDetailsForSubCategory(Request $request){
    	$rules =[
			        'sub_category_id' 	=> 	'required',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$response = [];
			if($request->sub_category_id==1){

				$home_types = DB::table('home_types')->where('home_type_subcategory', $request->sub_category_id)->get();
				$home_styles = DB::table('home_styles')->where('home_style_subcategory', $request->sub_category_id)->get();
				$response['home_types'] = $home_types;
				$response['home_styles'] = $home_styles;
				
			}else if($request->sub_category_id==2){
				$vehicle_types = DB::table('vehicle_types')->where('vehicle_type_subcategory', $request->sub_category_id)->get();
				$response['vehicle_types'] = $vehicle_types;
			}

			return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Details Fetched Successfully',
                  				'reponse_body' 	=> 	$response
                  			], 200);
		}
    }

    function lifeEventUpload(Request $request){
    	$rules =[
			        'type_id' 			=> 	'required',
			        'sub_category_id'	=>	'required',
			    ];
		if($request->type_id==1){
			if($request->sub_category_id==1){
				$rules['type_of_home'] = 'required';
				$rules['home_style'] = 'required';
			}else if($request->sub_category_id==2){
				$rules['tag_number'] = 'required';
				$rules['type_of_vehicle'] = 'required';
				$rules['vehicle_maker'] = 'required';
				$rules['vehicle_model'] = 'required';
				$rules['year_manufactured'] = 'required';
				$rules['vehicle_color'] = 'required';
			}
		}
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){

		}
    }
}
