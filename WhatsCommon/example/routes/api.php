<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\users;
use App\Http\Controllers\AppInformations;
use App\Http\Controllers\Settings;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post("login",[users::class,'getUsers']);
Route::post("registernew",[users::class,'registerUser']);
Route::post("regDetails",[users::class,'registerDetails']);
Route::post("forgotPass",[users::class,'checkVarifiedEmail']);
Route::post("updateDetails",[users::class,'updateProfile']);
Route::post("changePassword",[users::class,'ChnagePassword']);
Route::post("checkOtp",[users::class,'verifyOtp']);
Route::post("updateDp",[users::class,'updateImage']);
Route::post("newPass",[users::class,'newPassword']);
Route::post("setLang",[users::class,'SetUserLanguage']);
Route::post("setUserKeys",[users::class,'getUserKeywords']);
Route::post("feedback",[users::class,'submitFeedback']);
Route::post("connect",[users::class,'ConnectContact']);
Route::post("discovery",[users::class,'AllowDiscovery']);
Route::get("appInfo",[AppInformations::class,'GetAppInfo']);
Route::get("saveImage",[users::class,'getProfImage']);
Route::post("contact",[AppInformations::class,'ContactUs']);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post("get-financial-setting",[Settings::class,'findBankDetails']);
Route::post("set-financial-setting",[Settings::class,'addBankDetails']);
Route::post("donate",[Settings::class,'addDonation']);

Route::get("countries",[Settings::class,'getCountries']);
Route::post("provinces",[Settings::class,'getProvince']);
Route::post("cities",[Settings::class,'getCity']);
