<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\users;
use App\Http\Controllers\DbHandler;
use App\Http\Controllers\Register;
use App\Http\Controllers\Pages;
use App\Http\Controllers\Login;
use App\Http\Controllers\Master;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', function () {
    return view('index');
});
Route::get('login', function () {
    return view('login');
});
/*Route::get('signup', function () {
    return view('signup');
});*/
Route::get('signup', [Register::class,'index']);
/*Route::get('signup-details', function () {
    return view('signup_details');
});*/
Route::get('signup-details', [Register::class,'signupDetails']);
Route::get('forgot-password', function () {
    return view('forgot_password');
});
Route::post("register",[Register::class,'stepOne']);
Route::post("register-step-two",[Register::class,'stepTwo']);
Route::post("check-email",[Register::class,'checkEmail']);
Route::get('reset-password', function () {
    return view('set_password');
});
Route::post("set-password",[Register::class,'setPassword']);
//Route::get("userdat",[users::class,'printMsg']);

Route::get("userDet",[DbHandler::class,'getUsersFromDb']);


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
Route::get('admin/login', function () {
    return view('admin.login');
});
Route::get('admin/dashboard', function () {
    return view('admin.dashboard');
});

Route::get("admin/users",[users::class,'UsersList']);
Route::get("admin/pages",[Pages::class,'PagesList']);
Route::get("admin/edit-page/{_page_id}",[Pages::class,'EditPage']);
Route::post("admin/update-page",[Pages::class,'UpdatePage']);
Route::post("admin/login",[Login::class,'AdminLogin']);
Route::get("admin/genders",[Master::class,'GenderList']);
Route::get("admin/relations",[Master::class,'RelationList']);
Route::get("admin/provinces",[Master::class,'ProvincesList']);